import pandas as pd
import numpy as np
import tensorflow as tf

import itertools as it
import re
import time
import json
import argparse


parser = argparse.ArgumentParser('')
parser.add_argument('input_tfdataset')
args = parser.parse_args()

# Create a description of the features.
feature_description = {
    'meta': tf.io.FixedLenFeature([], tf.string),
    'fit': tf.io.FixedLenFeature([], tf.string),
    'noise': tf.io.FixedLenFeature([], tf.string)
    }
def _parse_function(example_proto):
    tmp = tf.io.parse_single_example(example_proto, feature_description)
    tmp['meta'] = tf.io.parse_tensor(tmp['meta'],tf.string)
    tmp['fit'] = tf.io.parse_tensor(tmp['fit'],tf.float32)
    tmp['noise'] = tf.io.parse_tensor(tmp['noise'],tf.float32)
    return tmp['meta'], tmp['fit'], tmp['noise']

#print("Each obs is",str(input_length),"variables")

def example_to_normal(adict):
    return adict['meta'],adict['fit'],adict['noise']

# Setting up real pipes, here's to filter
def filt_func(meta,fit,noise):
    return (
        tf.math.reduce_any(tf.math.is_nan(fit)) is not True
        )
#    .loc[lambda df: 
#            False == df["mutation_type"].isin( # it's the seventh in the array
#                ['other','NotSequenced','NotSequenced_adaptive','ExpNeutral','other_adaptive']
##                ['idontbelieveintheabovefilteringchoice']
#                )
#         ]


print(len(list(
tf.data.TFRecordDataset(args.input_tfdataset)
    .map(_parse_function)
    .filter(filt_func)
    )))

#good_observations = len(list(barcode_fit_error.filter(filt_func)))

#print("I see",str(good_observations),"filtered good obs")

## This just spits back the fitness twice
#def emit_fit_fit(meta,fitness,error):
#    return fitness, fitness
#
#input_length = list(barcode_fit_error.map(emit_fit_fit).take(1))[0][0].shape.as_list()[0]
#
## This resamples the mean fitness with the provided sample error as 
## population std deviation, I dunno
#def fitness_augmentation(meta,fitness,error):
#    fuzzed = tf.py_function(np.random.normal, [fitness, error], tf.float32) 
#    fuzzed.set_shape(fitness.shape)
#    return fuzzed, fuzzed
#
#fitness_profile_pairs_real = barcode_fit_error\
#        .filter(filt_func)\
#        .map(emit_fit_fit)\
#        .shuffle(buffer_size=good_observations)\
#        .batch(good_observations)
#        
#fitness_profile_pairs_fuzz = barcode_fit_error\
#        .filter(filt_func)\
#        .map(fitness_augmentation)\
#        .shuffle(buffer_size=good_observations)\
#        .batch(good_observations)
#        
#print("Each obs is",str(input_length),"variables")
#
#print( list(barcode_fit_error.filter(filt_func).map(fitness_augmentation).take(1)) )
##print( list(fitness_profile_pairs_real.take(1)) )
##print( list(fitness_profile_pairs_fuzz.take(1)) )
#
#
#
## Now the model... pretty much just retyping from https://github.com/keras-team/keras-io/blob/master/examples/generative/vae.py
## https://www.tensorflow.org/guide/keras/custom_layers_and_models#putting_it_all_together_an_end-to-end_example
#
#class Sampling(tf.keras.layers.Layer): 
#    """So this should take z_mean and z_log_var to create z, which gets decoded"""
#
#    @tf.function
#    def call(self, inputs):
#        z_mean, z_log_var = inputs
#        batch = tf.shape(z_mean)[0]
#        dim = tf.shape(z_mean)[1]
#        epsilon = tf.keras.backend.random_normal(shape=(batch,dim))
#        return z_mean + tf.exp(0.5*z_log_var) * epsilon # why this equation? look up normal dist....\n"
#class Encoder(tf.keras.layers.Layer):
#    """Take dimension of input and latent rep, return model. 1D inputs"""
#
#    def __init__(self, input_dim, latent_dim, layers_fore_aft=2, 
#            regularizer=tf.keras.regularizers.l1, regularization_parm=0.01,
#            name="encoder", **kwargs):
#
#        super().__init__(name=name, **kwargs)
#        self.input_dim = input_dim
#        self.latent_dim = latent_dim
#        self.layers_fore_aft = layers_fore_aft
#        self.regularizer = regularizer
#        self.regularization_parm = regularization_parm
#        
##        self.input_layer = tf.keras.Input(shape=(input_dim,))
#        
#        self.encoder_list = list()
#        for i in range(1,layers_fore_aft+1):
#            this_size = np.floor(input_dim -
#                    i*(input_dim-latent_dim)/(layers_fore_aft+1) )
#            print("Input layer",i,"is",this_size)
#            self.encoder_list.append( 
#                tf.keras.layers.Dense(this_size, activation='relu',
#                    kernel_regularizer=regularizer(regularization_parm)
#                    )
#                )
#        self.encoder_list.append(tf.keras.layers.Dense(latent_dim + latent_dim))
#        print("Encoded is 2 of ",latent_dim)
#        
#        self.sampling = Sampling()
#
#    def call(self, inputs):
#        x = inputs
#        for i in self.encoder_list:
#            x = i(x)
#        z_mean, z_logvar = tf.split(x, num_or_size_splits=2, axis=1)
#        z = self.sampling((z_mean, z_logvar))
#        return z_mean, z_logvar, z
#        
#class Decoder(tf.keras.layers.Layer):
#    """converts the z thing back into input"""
#    
#    def __init__(self, input_dim, latent_dim, layers_fore_aft=2, 
#            regularizer=tf.keras.regularizers.l1, regularization_parm=0.01,
#            name="decoder", **kwargs):
#
#        super().__init__(name=name, **kwargs)
#        self.input_dim = input_dim
#        self.latent_dim = latent_dim
#        self.layers_fore_aft = layers_fore_aft
#        self.regularizer = regularizer
#        self.regularization_parm = regularization_parm
#        
##        self.latent_layer = tf.keras.Input(shape=(latent_dim,))
#
#        self.decoder_list = list()
#        for i in range(1,layers_fore_aft+1):
#            this_size = np.floor(latent_dim +
#                    (i)*(input_dim-latent_dim)/(layers_fore_aft+1) )
#            print("Output layer",i,"is",this_size)
#            self.decoder_list.append( 
#                tf.keras.layers.Dense(this_size, activation='relu',
#                    kernel_regularizer=regularizer(regularization_parm)
#                    )
#                )
#        self.decoder_list.append(tf.keras.layers.Dense(input_dim))
#        print("Final output is ",input_dim)
#        
#    def call(self, inputs):
#        x = inputs
#        for i in self.decoder_list:
#            x = i(x)
#        return x
#
#
#class bowtie_vae(tf.keras.Model):
#    """makes end2end model...."""
#    
#    def __init__(self, input_dim, latent_dim, 
#                layers_fore_aft=2, 
#                regularizer=tf.keras.regularizers.l1, regularization_parm=0.01,
#                name="bowtie_vae", **kwargs
#                ):
#
#        super().__init__(name=name, **kwargs)
#        self.input_dim = input_dim
#        self.latent_dim = latent_dim
#        self.layers_fore_aft = layers_fore_aft
#        self.regularizer = regularizer
#        self.regularization_parm = regularization_parm
#        
#        self.encoder = Encoder(input_dim, latent_dim, layers_fore_aft, regularizer, regularization_parm)
#        self.decoder = Decoder(input_dim, latent_dim, layers_fore_aft, regularizer, regularization_parm)
#        
#    def call(self, inputs):
#        z_mean, z_logvar, z = self.encoder(inputs)
#        reconstructed = self.decoder(z)
#        kl_loss = -0.5 * tf.reduce_mean(z_logvar - tf.square(z_mean) - tf.exp(z_logvar) + 1)
#        self.add_loss(lambda x: kl_loss)
#        return reconstructed
#    
#    def train_step(self, inputs):
#        if isinstance(inputs, tuple): # oh because 
#            inputs = inputs[0] # don't understand
#        with tf.GradientTape() as tape: 
#            z_mean, z_logvar, z = self.encoder(inputs)
#            reconstruction = self.decoder(z)
#            reconstruction_loss = tf.reduce_mean(
#                tf.keras.losses.mean_squared_error(inputs, reconstruction)
#                )
#            reconstruction_loss *= self.input_dim # not sure what this is doing, but I think it's scaling the loss?
#            kl_loss = -0.5 * tf.reduce_mean(z_logvar - tf.square(z_mean) - tf.exp(z_logvar) + 1)
#            total_loss = reconstruction_loss + kl_loss
#        # This indenting strikes me as odd, but it is because tape is watching the variables automatically here, opaquely
#        grads = tape.gradient(total_loss, self.trainable_weights) 
#        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
#
#        return {
#            "loss": total_loss,
#            "reconstruction_loss": reconstruction_loss,
#            "kl_loss": kl_loss
#            }
#    
#    def test_step(self, inputs):
#        if isinstance(inputs, tuple): # don't understand
#            inputs = inputs[0] # don't understand
#        z_mean, z_logvar, z = self.encoder(inputs)
#        reconstruction = self.decoder(z)
#        reconstruction_loss = tf.reduce_mean(
#            tf.keras.losses.mean_squared_error(inputs, reconstruction)
#            )
#        reconstruction_loss *= self.input_dim # not sure what this is doing, but I think it's scaling the loss?
#        kl_loss = -0.5 * tf.reduce_mean(z_logvar - tf.square(z_mean) - tf.exp(z_logvar) + 1)
#        total_loss = reconstruction_loss + kl_loss
#            
#        return {
#            "loss": total_loss,
#            "reconstruction_loss": reconstruction_loss,
#            "kl_loss": kl_loss
#            }
#
#good_observations = len(list(barcode_fit_error.filter(filt_func)))
#print("I see",str(good_observations),"filtered good obs")
#
#def emit_fit_fit(meta,fitness,error):
#    return fitness, fitness
#
#input_length = list(barcode_fit_error.map(emit_fit_fit).take(1))[0][0].shape.as_list()[0]
#
#def fitness_augmentation(meta,fitness,error):
#    fuzzed = tf.py_function(np.random.normal, [fitness, error], tf.float32) 
#    fuzzed.set_shape(fitness.shape)
#    return fuzzed, fuzzed
#
#fitness_profile_pairs_real = barcode_fit_error\\
#        .filter(filt_func)\\
#        .map(emit_fit_fit)\\
#        .shuffle(buffer_size=good_observations)\\
#        .batch(good_observations)
#        
#fitness_profile_pairs_fuzz = barcode_fit_error\\
#        .filter(filt_func)\\
#        .map(fitness_augmentation)\\
#        .shuffle(buffer_size=good_observations)\\
#        .batch(good_observations)
#        
#print("Each obs is",str(input_length),"variables")
#
#print( list(barcode_fit_error.filter(filt_func).map(fitness_augmentation).take(1)) )
##print( list(fitness_profile_pairs_real.take(1)) )
##print( list(fitness_profile_pairs_fuzz.take(1)) )"
#
#parms = {
#        'layers_fore_aft':[2,3], #1 2 3
#        'latent_dim':[2,4,8,12,16,32], #[2,4,6,8,10,12,16,20,24,32,64,128],
#        'learning_rate':[1e-2,1e-3,1e-4], #[1e-4,1e-3,1e-2],
#        'regularization_parm':[1e-1,1e-2,1e-3], #[0,1e-3,1e-2,1e-1],
#        'augmented':[True, False],
#    }
#parms
#
#models = dict()
#histories = dict()
#
#for i in it.product(*[parms[j] for j in parms] ):
#
#    these_parms = dict(zip(parms,i))
#    parm_string = "_".join(["_".join([k,str(v)]) for k,v in these_parms.items()])
#    
#    print("BEGINNING")
#    print(these_parms)
#
#    models[parm_string] = bowtie_vae(
#            input_dim=input_length,
#            latent_dim=these_parms['latent_dim'], 
#            layers_fore_aft=these_parms['layers_fore_aft'],
#            regularizer=tf.keras.regularizers.l1,
#            regularization_parm=these_parms['regularization_parm']
#            )
#
#    models[parm_string].build((1,input_length))
#    
#    models[parm_string].compile(
#            optimizer=tf.keras.optimizers.Adam(these_parms['learning_rate']),
#            loss=tf.keras.losses.MeanSquaredError()
#            )
#    
#    checkpoint   = tf.keras.callbacks.ModelCheckpoint(
#        filepath='../best_models/'+parm_string+'.chkpt',
#        monitor='loss', verbose=0, save_best_only=True, 
#        save_weights_only=False, mode='min')
#    tensor_board = tf.keras.callbacks.TensorBoard(
#            log_dir="../kboard/"+parm_string,histogram_freq=0,
#            write_graph=True, write_images=True)
#    history      = tf.keras.callbacks.History()
#    
#    if these_parms['augmented']:
#
#        early_stop   = tf.keras.callbacks.EarlyStopping('val_loss',
#            patience=100, mode='min')
#        
#        models[parm_string]\\
#            .fit(
#                fitness_profile_pairs_fuzz,
#                epochs=10000, #steps_per_epoch=10,
#                validation_data=fitness_profile_pairs_real,
#                validation_freq=1,
#                callbacks=[early_stop, checkpoint, history, tensor_board] 
#                )
#
#    else:
#
#        early_stop   = tf.keras.callbacks.EarlyStopping('loss',
#            patience=100, mode='min')
#
#        models[parm_string]\\
#            .fit(
#                fitness_profile_pairs_real,
#                epochs=10000, #steps_per_epoch=10,
#                callbacks=[early_stop, checkpoint, history, tensor_board] 
#                )
#            
#    with open('../histories/200920/'+parm_string+'.json','w') as f:
#        json.dump(history.history,f)
#
#    print(these_parms)
#    print("ENDING\n\n\n")"
##models['layers_fore_aft_2_latent_dim_8_learning_rate_0.001_regularization_parm_0.01_augmented_False'].layers[0]"
## TODO normalize per-experiment measures ?
## TODO figure way to plot it, check it out, see how it looks
## TODO see if we can pull out the encoded dimension, plot it, see how barinfo data looks in that space"
#
#
