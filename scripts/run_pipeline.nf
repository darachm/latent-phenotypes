/*

Yep nextflow for launching model fitting jobs

*/

file("tmp").mkdirs()
file("reports").mkdirs()

bigbatch_fitness_csv = Channel
    .fromPath("1BigBatch/data/fitness_weighted_allconditions_swapsremoved_neutral2xpass.csv")

process preprocess_bigbatch_dataset {
    memory '4 GB'
    publishDir 'tmp'
    label 'python3'
    input:
        file(script) from Channel.fromPath("scripts/preproc_bigbatch.py")
        file(csv) from bigbatch_fitness_csv
    output:
        file("tfdataset.serial") into preproc
    shell:
    '''
    python3 !{script} !{csv}
    #jupyter nbconvert --to=html --ExecutePreprocessor.timeout=-1 --execute !{script}
    '''
}


process fit_bowtieVae {
    memory '4 GB'
    publishDir 'tmp'
    label 'python3'
    input:
        file(script) from Channel.fromPath("scripts/fit_bowtieVae.py")
        file(tfdataset) from preproc
    output:
    shell:
    '''
    export TF_CPP_MIN_LOG_LEVEL=1
    python3 !{script} !{tfdataset}
    #jupyter nbconvert --to=html --ExecutePreprocessor.timeout=-1 --execute !{script}
    '''
}


//dense_connected = Channel.fromPath("scripts/dense_connected.ipynb")
//process try_dense {
//    memory '8 GB'
//    publishDir 'tmp'
//    container "file://singularity_containers/jupyter.simg"
//    input:
//        set file(csv), file(pkl) from his3_preproc
//        file script from dense_connected
//    output:
//        file("*.html") into dense_report
//    shell:
//    '''
//    jupyter nbconvert --to=html --ExecutePreprocessor.timeout=-1 --execute !{script}
//    '''
//}
//    "parms = {\n",
//    "        'layers_fore_aft':[2,3], #1 2 3\n",
//    "        'latent_dim':[2,4,8,12,16,32], #[2,4,6,8,10,12,16,20,24,32,64,128],\n",
//    "        'learning_rate':[1e-2,1e-3,1e-4], #[1e-4,1e-3,1e-2],\n",
//    "        'regularization_parm':[1e-1,1e-2,1e-3], #[0,1e-3,1e-2,1e-1],\n",
//    "        'augmented':[True, False],\n",
//    "    }\n",
//    "parms\n",
