import pandas as pd
import numpy as np
import tensorflow as tf
#tf.logging.set_verbosity(tf.logging.ERROR)

import argparse

parser = argparse.ArgumentParser('')
parser.add_argument('input_csv')
args = parser.parse_args()

#import re

# I looked at the data filtering steps from Kinsler's jupyter note book

input_datar = pd.read_csv(args.input_csv) \
    .replace([np.inf, -np.inf], np.nan) \
    .dropna() 

print(input_datar.shape)

print(input_datar.iloc[0:3,:])

# Which columns are metadata, that I want to save for later analyses?
meta_columns = ["barcode", "gene", "type", "ploidy", "class", 
    "additional_muts", "mutation_type" ]

# List of all columns
columnz = meta_columns+ \
    sorted(input_datar.loc[:,input_datar.columns.str.contains("_fitness")]) + \
    sorted(input_datar.loc[:,input_datar.columns.str.contains("_error")])

# Now pull out three lists, of the indicides for metadat, for fitnesses, or for error
indicies = [
    input_datar.loc[:,columnz].columns.isin(meta_columns),
    input_datar.loc[:,columnz].columns.str.contains("_fitness"),
    input_datar.loc[:,columnz].columns.str.contains("_error")
    ]
fitz_nan = input_datar.apply( lambda x: x.loc[columnz].loc[indicies[1]],axis=1)\
    .apply(lambda x: x.isna().any() == False, axis=1 )
error_nan = input_datar.apply( lambda x: x.loc[columnz].loc[indicies[2]],axis=1)\
    .apply(lambda x: x.isna().any() == False, axis=1 )

# Okay, now zip together three tf Datasets that are each of these, 
# so one is metadata, one is fitnesses, one is error
# This is our base dataset, then we filter it later
barcode_fit_error = \
    tf.data.Dataset.zip(
        (
            tf.data.Dataset.from_tensor_slices(
                input_datar.apply( lambda x: x.loc[columnz].loc[indicies[0]],axis=1)\
                    .to_numpy(dtype=np.string_)[fitz_nan | error_nan]
                )
            ,
            tf.data.Dataset.from_tensor_slices(
                input_datar.apply( lambda x: x.loc[columnz].loc[indicies[1]],axis=1)\
                    .to_numpy(dtype=np.float32)[fitz_nan | error_nan]
                )
            ,
            tf.data.Dataset.from_tensor_slices(
                input_datar.apply( lambda x: x.loc[columnz].loc[indicies[2]],axis=1)\
                    .to_numpy(dtype=np.float32)[fitz_nan | error_nan]
                )
            )
        )

# gawd this is ugly copypasta

def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def serialize_example(meta,fit,noise):
    feature = {
        'meta': _bytes_feature(tf.io.serialize_tensor(meta)),
        'fit': _bytes_feature(tf.io.serialize_tensor(fit)),
        'noise': _bytes_feature(tf.io.serialize_tensor(noise))
        }
    return tf.train.Example(features=tf.train.Features(feature=feature)).SerializeToString()

with tf.io.TFRecordWriter('tfdataset.serial') as f:
    for i,j,k in barcode_fit_error:
        f.write(serialize_example(i,j,k))

## Create a description of the features.
#feature_description = {
#    'meta': tf.io.FixedLenFeature([], tf.string),
#    'fit': tf.io.FixedLenFeature([], tf.string),
#    'noise': tf.io.FixedLenFeature([], tf.string)
#    }
#def _parse_function(example_proto):
#    tmp = tf.io.parse_single_example(example_proto, feature_description)
#    print(tmp)
#    tmp['meta'] = tf.io.parse_tensor(tmp['meta'],tf.string)
#    tmp['fit'] = tf.io.parse_tensor(tmp['fit'],tf.float32)
#    tmp['noise'] = tf.io.parse_tensor(tmp['noise'],tf.float32)
#    return tmp
#for i in tf.data.TFRecordDataset('tfdataset.serial').map(_parse_function):
#    print(i)


