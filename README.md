




Or also, ssh in and then do

    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/cuda-10.1/extras/CUPTI/lib64; nohup jupyter notebook --no-browser --notebook-dir / &
    nohup python3 ~/.local/lib/python3.6/site-packages/tensorboard/main.py --logdir=/mnt/scratch/latent-phenotypes/kboard &
    ssh -L localhost:8888:localhost:8888 -L localhost:6006:localhost:6006 ms
