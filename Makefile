.PHONY: all clean archive

all: pipeline

pipeline: scripts/nextflow scripts/run_pipeline.nf \
		scripts/run_pipeline.nfconfig
	unset SINGULARITY_CACHEDIR && \
		$(word 1,$^) run $(word 2,$^) -c $(word 3,$^) \
                -resume \
				-ansi-log false \
                -with-dag reports/dag.html











scripts/nextflow:
	cd scripts && curl -s https://get.nextflow.io | bash

archive: data.zip
data.zip: data
	zip -9 -r $@ $<

clean:
	@echo "do a clean? if not cancel C^+C or something"
	@read DO_IT
	rm -r .nextflow* reports work tmp
	rm -rf scripts/.ipynb_checkpoints
	rm -rf scripts/*.png
	rm -rf scripts/*.pdf
	rm -rf scripts/*.jpeg
	rm -rf scripts/*_cache
	rm -rf scripts/*_files

# Here's a rule to just convert PDFs to PNGs for good sharing
output/%.png: output/%.pdf
	convert -density 300 $< $@
# Here's a rule to just convert PDFs to JPGs for easy sharing
output/%.jpg: output/%.pdf
	convert $< $@
